module.exports = {
    network: 'regtest',
    maxConf: 2,
    electrum: {
        host: 'localhost',
        port: 50001,
        proto: 'tcp',
        // host: 'electrum.bip.click',
        // port: 50002,
        // proto: 'tls',
        minProtocol: '1.4',
        clientVersion: '3.3.6'
    },
    bitcoin: {
        port: 18443,
        username: 'user',
        password: 'pass'
    }
};
