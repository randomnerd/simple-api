## Stage 1
Using nodejs, bitcoind, electrumx implement 5 functions specified below.
- [electrumx](https://github.com/kyuupichan/electrumx)
- [bitcoind](https://github.com/bitcoin/bitcoin)


#### async send( amount:float, address:string, fee:number ) -> txId:string
#### DONE
- amount - amount of funds
- address - where funds are beign sent to
- fee - satoshi per byte
- txId - id of the created transaction on the network

#### async feeRegular() -> fee:number
#### DONE
- fee - regular fee/byte for transaction

#### async feePriority() -> fee:number
#### DONE
- fee - priority fee/byte for transaction

#### async createAlias() -> address:string
#### DONE
- address - Bitcoin address of the created alias

#### async balance() -> balance:float
#### DONE
## Stage 2

Implement the callback function that would be called every time wallet receives a transaction or transaction receives confirmation up to config.MAX_CONFIRMATIONS

#### async callbackReceivedTransaction( txId:string, addressTo:string, amount:float, confirmations:number )
#### DONE
- txId - Id of the received transaction
- addressTo - Address of the receiver
- amount - Amount of the received transaction
- confirmations - Number of confirmations on the network

The callback function doesn't need to do anything.

## Stage 3

Describe the process of wallet creation and how to receive its xpub token.
