import bitcoin from 'bitcoinjs-lib';
import crypto from 'crypto';
import config from 'config'

const network = bitcoin.networks[config.network];

export function newRoot() {
    const seed = crypto.randomBytes(64);
    return bitcoin.bip32.fromSeed(seed, network);
}

export function loadRoot(key) {
    return bitcoin.bip32.fromBase58(key, network);
}

export function getAlias(root, i = 0) {
    const child = root.derivePath(`m/${i}'`);
    const { address, script, redeemScript } = getAddressAndScript(child);
    const key = child.toBase58();
    const xpub = child.neutered().toBase58();
    return { key, script, redeemScript, address, xpub };
}

export function getAliasFromPrivKey(rootkey, i = 0) {
    const root = loadRoot(rootkey);
    return getAlias(root, i);
}

export function hashX(script) {
    return bitcoin.crypto.sha256(script).reverse().toString('hex');
}

export function addrToScript(address) {
    return bitcoin.address.toOutputScript(address, network);
}

export function scriptToAddr(script) {
    // return bitcoin.address.fromOutputScript(script, network);
    if (typeof script === 'string') script = Buffer.from(script, 'hex');
    console.log(script.toString('hex'));
    const pubkey = bitcoin.ECPair.fromPublicKey(script, {network});

    const { address } = bitcoin.payments.p2sh({
        network,
        redeem: bitcoin.payments.p2wpkh({ pubkey: script, network })
    });
    return address;
}

export function addrToHashX(address) {
    const script = addrToScript(address);
    return hashX(script);
}

export function getAddressAndScript(node) {
    const { address, output, redeem } = bitcoin.payments.p2sh({
        network,
        redeem: bitcoin.payments.p2wpkh({ pubkey: node.publicKey, network })
    });
    return { address, script: output.toString('hex'), redeemScript: redeem.output.toString('hex') };
}
