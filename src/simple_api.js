import bitcoin from 'bitcoinjs-lib';
import ec from '../electrumjs/dist/main';
import { newRoot, loadRoot, getAliasFromPrivKey, getAlias, addrToHashX, addrToScript, scriptToAddr } from './lib/hash';
import config from 'config';
import coinselect from 'coinselect';

// dirty hack
const ElectrumClient = typeof ec === 'function' ? ec : ec.default;

export default class SimpleApi {
    constructor(lastHeight = 0) {
        this.lastHeight = lastHeight;
        const { host, port, proto } = config.electrum;
        this.ecl = new ElectrumClient(host, port, proto);
        this.network = bitcoin.networks[config.network];
        this.pendingTxs = {};
        this.txCache = {};
        this.subscriptions = {};
        this.txCache = {};
        this.hashx = {};
    }

    newWallet() {
        const root = newRoot();
        console.log(`root key: ${root.toBase58()}`);
        return newRoot();
    }

    loadWallet(privkey) {
        return loadRoot(privkey);
    }

    getAlias(root, aliasId) {
        return getAlias(root, aliasId);
    }

    getAliasFromPrivkey(pk, aliasId) {
        return getAliasFromPrivKey(pk, aliasId);
    }

    watchAddress(address, callback) {
        if (typeof callback !== 'function') throw new Error('callback must be a function');
        const hashx = addrToHashX(address);
        this.hashx[hashx] = { address, script: addrToScript(address).toString('hex') };
        // console.log(hashx);
        if (!this.subscriptions[address]) this.subscriptions[address] = [];
        this.subscriptions[address].push(callback);
        this.ecl.methods.blockchain_scripthash_subscribe(hashx);
    }

    async onTx(payload) {
        const [hashx, status] = payload;
        const { script, address: addressTo } = this.hashx[hashx];
        const callbacks = this.subscriptions[addressTo];
        console.log('ontx', addressTo, script);
        if (!callbacks || !callbacks.length) return;

        const history = await this.ecl.methods.blockchain_scripthash_getHistory(hashx);
        const txids = history.filter(i => i.height <= 0).map(i => i.tx_hash);
        await Promise.all(txids.map(async (txid) => {
            const tx = await this.getCachedTx(txid);
            const txDelta = await this.getTxDelta(tx);
            const payload = {
                txid,
                addressTo,
                amount: txDelta ? txDelta[script] : 0,
                confirmations: 0
            };
            this.pendingTxs[txid] = payload;
            return Promise.all(callbacks.map(async (cb) => Promise.resolve(cb(payload))));
        }));
    }

    async getTxDelta(tx) {
        try {
            const delta = tx.delta = {};
            await Promise.all(tx.vin.map(async (pl) => {
                // console.log(pl);
                const { txid, vout } = pl;
                if (!txid || txid === '0'.repeat(64)) return;
                const intx = await this.getCachedTx(txid)
                const { value, scriptPubKey: spk } = intx.vout[vout];
                // console.log(intx.vout[vout]);
                // if (value <= 0) return;
                if (delta[spk.hex] === undefined) delta[spk.hex] = 0;
                delta[spk.hex] -= value;
            }));
            tx.vout.forEach(({ value, scriptPubKey: spk }) => {
                // if (value <= 0) return ;
                if (delta[spk.hex] === undefined) delta[spk.hex] = 0;
                delta[spk.hex] += value;
            });
            console.log(delta);
            return delta;
        } catch (error) {
            console.error(error);
        }
    }

    async init() {
        try {
            await this.ecl.connect();
            const { clientVersion, minProtocol } = config.electrum;
            await this.ecl.methods.server_version(clientVersion, minProtocol);
            this.ecl.events.on('blockchain.headers.subscribe', this.onBlockHeader.bind(this));
            this.ecl.events.on('blockchain.scripthash.subscribe', this.onTx.bind(this));

            const { height } = await this.ecl.methods.blockchain_headers_subscribe();
            // const header = await this.ecl.methods.blockchain_block_header(height);
            // console.log(height, header);
            // // const block = bitcoin.Block.fromHex(hex);
            // const txids = await this.getBlockTxIds(height);
            // txids.forEach(async (txid, idx) => {
            //     console.log(`parsing ${txid} at pos ${idx}`)
            //     try {
            //         const rawtx = await this.ecl.methods.blockchain_transaction_get(txid)
            //         const tx = bitcoin.Transaction.fromHex(rawtx);
            //         console.log('parsed');
            //     } catch (err) {
            //         console.log(`error parsing ${txid} @ ${idx}`)
            //         // console.error(err)
            //     }
            // })
            // console.log(txids);
            // this.ecl.events.on('blockchain.headers.subscribe', this.onBlockHeader.bind(this));
        } catch (err) {
            console.error(err);
        }
    }

    async getCachedTx(txid) {
        if (!this.txCache[txid]) this.txCache[txid] = await this.ecl.methods.blockchain_transaction_get(txid, true);
        return this.txCache[txid];
    }

    async onBlockHeader(payload) {
        try {
            const { height } = payload[0];
            await Promise.all(Object.keys(this.pendingTxs).map(async (txid) => {
                const payload = this.pendingTxs[txid];
                const tx = await this.ecl.methods.blockchain_transaction_get(txid, true);
                payload.confirmations = tx.confirmations;
                if (payload.confirmations >= config.maxConf) delete this.pendingTxs[txid];
                const callbacks = this.subscriptions[payload.addressTo];
                if (!callbacks || !callbacks.length) return;
                return Promise.all(callbacks.map(async (cb) => Promise.resolve(cb(payload))));
            }));
        } catch (error) {
            console.error(error);
        }
    }

    feeRegular() {
        return this.ecl.methods.blockchain_estimatefee(5);
    }

    feePriority() {
        return this.ecl.methods.blockchain_estimatefee(1);
    }

    balance(address) {
        const script = bitcoin.address.toOutputScript(address, this.network);
        return this.ecl.methods.blockchain_scripthash_getBalance(script)
    }

    async buildTx({ source, target, value, feeRate }) {
        console.log(source.address, target, value, feeRate);
        const keyPair = bitcoin.bip32.fromBase58(source.key, this.network);
        const script = bitcoin.address.toOutputScript(target, this.network);
        const targets = [{ script, value }];
        const hashx = bitcoin.crypto.sha256(Buffer.from(source.script, 'hex')).reverse().toString('hex');

        const txb = new bitcoin.TransactionBuilder(this.network);
        const utx = await this.ecl.methods.blockchain_scripthash_listunspent(hashx);
        if (!utx.length) return;
        const utxo = utx.map(({ tx_hash: txId, tx_pos: vout, value }) => ({
            txId,
            vout,
            value,
        }));
        const { inputs, outputs, fee } = coinselect(utxo, targets, feeRate);
        inputs.forEach(input => txb.addInput(input.txId, input.vout))
        outputs.forEach(output => {
            if (!output.script) output.script = source.script;
            txb.addOutput(Buffer.from(output.script, 'hex'), output.value)
        });
        inputs.forEach((i, vin) => txb.sign({
            vin,
            keyPair,
            witnessValue: inputs[vin].value,
            prevOutScriptType: 'p2sh-p2wpkh',
            redeemScript: Buffer.from(source.redeemScript, 'hex')
        }));
        const signed = txb.build();
        signed.fee = fee;
        return signed;
    }

    sendTx(tx) {
        const hex = typeof tx === 'string' ? tx : tx.toHex();
        return this.ecl.methods.blockchain_transaction_broadcast(hex);
    }
}