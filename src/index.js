import SimpleApi from './simple_api';

const api = new SimpleApi();

async function main() {
    await api.init();
    const root = api.loadWallet('tprv8ZgxMBicQKsPefv9aPHAjJSRBPwhqhfoUXrrNvK5o7SNvJHdi3GGgDfTKo56pRYyfjRnkHmEgQSHVDZip6A1iMXF5tbj9jzBTr2u4kTJ5wU', api.network);
    // const root = api.newWallet();
    const alias = api.getAlias(root, 0);
    console.log(alias);
    api.watchAddress(alias.address, payload => {
        const { txid, addressTo, amount, confirmations } = payload;
        console.log(payload);
    });
    const newTx = await api.buildTx({
        source: alias,
        target: '2NAJB4fw9v5WgQbRNYKqN6nQQnUvEPy3zHT',
        value: 0.01 * 10**8,
        feeRate: 55
    });
    console.log(newTx.fee);
    console.log(await api.sendTx(newTx));
}

main().catch(console.error);
